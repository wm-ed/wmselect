import '../scss/index.scss';
import axios from 'axios';
import WMSelect from '../../../dist/wmselect.bundle.js';

const marks = document.getElementById('marks');
const reset = document.getElementById('reset');
const submit = document.getElementById('card');

new WMSelect(marks, {
    search: {
        turnOn: true,
    }
});

reset.addEventListener('click', e => {
    marks.wmreset();
})

submit.addEventListener('submit', e => {
    e.preventDefault();
    console.log(marks.value);
})

// const models = document.getElementById('models');
// const types = document.getElementById('types');
// const categories = document.getElementById('categories');

// new WMSelect(marks, {
//     search: { turnOn: true, },
// });

// marks.addEventListener("wmselect:change", function(e) {
//     models.wmreset({ disabled: true, });
//     types.wmreset({ disabled: true, });
//     categories.wmreset({ disabled: true, });
//     models.wmopen({

//     });
// })

// new WMSelect(models, {
//     search: { turnOn: true, },
//     parentDisabled: '#marks',
//     iconSelection: {
//         type: 'img',
//         source: './img/acura.png',
//     },
//     ajax ({ resolve, params }) {
//         console.log(params)
//         const data = { data: [] };
//         delete axios.defaults.headers.common['X-Requested-With'];
//         axios
//         .get('https://api.discogs.com/artists/391170/releases?token=QBRmstCkwXEvCjTclCpumbtNwvVkEzGAdELXyRyW')
//         .then(response => {
//             data.data.push(
//                 {
//                     title: 'Круті марки',
//                     children: [
//                         {
//                             img: {
//                                 type: 'img',
//                                 source: './img/alfa-romeo.png',
//                             },
//                             value: 'alfa1',
//                             text: {
//                                 text: 'ALFA ROMEO GG TT alfa romeo',
//                                 years: '2021',
//                             },
//                         },
//                         {
//                             value: 'alfa2',
//                             text: 'ALFA ROMEO 2',
//                         },
//                     ]
//                 },
//                 {
//                     title: 'Інші марки',
//                     children: [
//                         {
//                             img: {
//                                 type: 'img',
//                                 source: './img/citroen.png'
//                             },
//                             value: 'citroen1',
//                             text: 'CITROEN',
//                         },
//                         {
//                             img: {
//                                 type: 'img',
//                                 source: './img/citroen.png'
//                             },
//                             value: 'citroen2',
//                             text: {
//                                 years: '2014 citroen'
//                             },
//                         },
//                         {
//                             img: {
//                                 type: 'img',
//                                 source: './img/nissan.png',
//                             },
//                             value: 'nissan1',
//                             text: 'NISSAN',
//                         },
//                     ]
//                 },
//             );
//             data.currentPage = 2;
//             data.lastPage = 3;
//             resolve(data);
//         })
//     },
//     templateResult (state) {
//         let result = {};
//         if (typeof state.text === 'string') result.text = state.text;
//         if (typeof state.text === 'object') {
//             result.text = state.text.text;
//             result.years = state.text.years;
//         }
//         return `
//             ${!!state.img ? `<span class="img">${state.img}</span>`: ''}
//             ${!!result.text ? result.text : ''} ${!!result.years ? `<span class="static-years">${result.years}</span>` : ''}
//         `.trim();
//     },
// });

// models.addEventListener("wmselect:change", function(e) {
//     types.wmreset({ disabled: true, });
//     categories.wmreset({ disabled: true, });
//     types.wmopen();
// })

// new WMSelect(types, {
//     search: { turnOn: true, },
//     parentDisabled: '#models',
// });

// types.addEventListener("wmselect:change", function(e) {
//     categories.wmreset({ disabled: true, });
//     categories.wmopen();
// })

// new WMSelect(categories, {
//     search: { turnOn: true, },
//     parentDisabled: '#types',
// });

// const test = document.getElementById('test')

// new WMSelect(test, {
//     search: { turnOn: true },
// });

// const test2 = document.getElementById('test2')

// new WMSelect(test2, {
//     search: { turnOn: true },
// });