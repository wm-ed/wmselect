import Default from '../config/default.js';

let intervalInput;

document.addEventListener('click', (e) => {
    Array.from(document.querySelectorAll('.wmselect.active'), custom => {
        if (e.target == custom || !e.target.closest('.wmselect')) {
            clearInterval(intervalInput);
            custom.classList.remove('active');
            if (!!custom.querySelector('.wmselect__input')) custom.querySelector('.wmselect__input').value = '';
            const id = custom.getAttribute('data-custom');
            const native = document.querySelector(`select[data-native="${id}"]`);
            native.dispatchEvent(new CustomEvent("wmselect:wmclose"));
        }
    });
});

export default class WMSelect {
    constructor (native, data = {}) {

        if (native === null) throw new Error(Default.errors.init);

        this.native = native;
        this.id = `wmselect-${Default.uniqueId[Default.uniqueId.length - 1]}`;

        this.native.setAttribute('data-native', this.id);
        Default.uniqueId.push(Default.uniqueId[Default.uniqueId.length - 1] + 1);

        this.config = {};
        this.config.currentPage = 1;
        this.config.lastPage = (() => {
            if (this.native.hasAttribute('data-default-lastpage')) {
                return parseInt(this.native.getAttribute('data-default-lastpage'));
            }
            return null;
        })();
        this.config.search = (() => {
            const isTurnOn = !!data.search && data.search.turnOn === true;
            const isMinLength = !!data.search && typeof data.search.minLength === 'number';
            return {
                turnOn: isTurnOn,
                minLength: isTurnOn && isMinLength ? search.min : Default.searchMinLength,
            }
        })();
        this.config.showLine = data.showLine;
        this.config.parent = data.parentDisabled;
        this.config.intervalInput = !!data.intervalInput ? data.intervalInput : Default.intervalInput;
        this.config.reverseIconSelection = !!data.reverseIconSelection && typeof data.reverseIconSelection == 'boolean' ? true : undefined;
        this.config.langSelector = !!data.langSelector && data.langSelector;
        this.config.params = {};

        this.ajax = data.ajax;
        this.templateResult = data.templateResult;
        this.templateSelection = data.templateSelection;
        this.iconSelection = !!data.iconSelection && data.iconSelection;

        this.iconSelectionArrow = !!data.iconSelectionArrow && data.iconSelectionArrow;
        this.options = {};
        this.selected = {
            img: { type: '', source: '' },
            value: '',
            text: ''
        };
        this.direction = 0;
        this.stopInfinity = true;

        this.lang = {};
        this.toFormLang();

        this.custom = this.createCustomSelect();
        this.native.classList.add('wmnative-hide');

        this.dom = {};
        this.dom.selection = this.custom.querySelector('.wmselect__selection');
        this.dom.selected = this.custom.querySelector('.wmselect__selected');
        this.dom.input = this.custom.querySelector('.wmselect__input');
        this.dom.dropdown = this.custom.querySelector('.wmselect__dropdown');
        this.dom.inner = this.custom.querySelector('.wmselect__inner');
        this.dom.options = this.custom.querySelector('.wmselect__options');
        this.dom.icon = this.custom.querySelector('.wmselect__icon');

        this.isDisabled = false;
        this.disabledSelect();

        this.native.wmclose = () => (this.wmclose());
        this.native.wmopen = (params = {}) => (this.wmopen(params));
        this.native.wmchange = (value) => (this.wmchange(value));
        this.native.wmreset = (params) => (this.wmreset(params));

        this.custom.addEventListener('click', this.listenerClick.bind(this));
        this.config.search.turnOn && this.custom.addEventListener('input', this.listenerInput.bind(this));
        !!this.ajax && this.dom.options.addEventListener('scroll', this.listenerScroll.bind(this));
    }
    setActiveOption () {
        if (this.selected.value.length) {
            const option = this.dom.options.querySelector(`.wmselect__option[data-value="${this.selected.value}"]`);
            !!option && option.classList.add('active');
        }
    }
    toFormLang () {
        const selector = document.querySelector(this.config.langSelector);
        Default.lang.forEach(lang => {
            const attrNative = this.native.getAttribute(`data-${lang.attr}`);
            const attrSelector = (() => {
                if (this.config.langSelector) {
                    const selector = document.querySelector(this.config.langSelector);
                    const attr = selector.getAttribute(`data-${lang.attr}`);
                    return attr;
                }
                return;
            })();
            if (attrNative !== null) {
                this.lang[lang.attr] = attrNative;
            }
            else if (!!selector && attrSelector !== null) {
                this.lang[lang.attr] = attrSelector;
            }
            else {
                this.lang[lang.attr] = lang.text;
            }
        })
    }
    getNativeOptions () {
        const listData = [];
        const setObjectData = ((option) => {
            let setData = {};
            setData.value = !!option.getAttribute('value') ? option.getAttribute('value') : '';
            const imgFormat = option.dataset.defaultType;
            const imgData = option.dataset.defaultSource;
            if (!!imgFormat && !!imgData) {
                setData.img = {};
                setData.img.type = imgFormat;
                setData.img.source = imgData;
            }
            for (const attr in option.dataset) {
                const getAttr = attr.replace(/default/, '').toLowerCase();
                if (getAttr !== 'type' && getAttr !== 'source') {
                    setData.text = {};
                    setData.text[getAttr] = option.dataset[attr];
                    setData.text.text = option.innerText;
                }
            }
            if (!setData.text) setData.text = option.innerText;
            return setData;
        });
        Array.from(this.native.getElementsByTagName('option'), option => {
            if (option.value.length) {
                const data = setObjectData(option);
                listData.push(data);
            }
        });
        if (listData.length) {
            const index = this.native.options.selectedIndex;
            const data = setObjectData(this.native.options[index]);
            this.selected = {
                img: {
                    type: !!data.img && data.img.type ? data.img.type : '',
                    source: !!data.img && data.img.source ? data.img.source : '',
                },
                value: data.value,
                text: data.text,
            };
        }
        this.options = listData;
        return listData;
    }
    setSelection () {
        let template = {};
        template.icon = (() => {
            if (this.config.reverseIconSelection === true && !!this.iconSelection) {
                if (this.selected.img.type.length) {
                    return this.createIcon(this.selected.img);
                }
                return this.createIcon(this.iconSelection);
            } else if (this.config.reverseIconSelection === undefined && !!this.iconSelection) {
                return this.createIcon(this.iconSelection);
            }
        })();
        if (!!this.templateSelection) {
            template.selected = this.templateSelection({ text: this.selected.text });
        }
        if (!template.selected) {
            let give = {};
            if (typeof this.selected.text === 'string') give.text = this.selected.text;
            if (typeof this.selected.text === 'object') give = this.selected.text;
            template.selected = typeof give == 'object' ? Object.values(give).join(' ') : give;
        }
        template.title = (() => {
            if (typeof this.selected.text === 'string') {
                return this.selected.text;
            }
            if (typeof this.selected.text === 'object') {
                return Object.values(this.selected.text).join(' ');
            }
        })();
        return template;
    }
    createIcon (icon) {
        const isIcon = !!icon.type && !!icon.source;
        let template = '';
        if (isIcon) {
            switch (icon.type) {
                case 'img':
                    template = `<img src="${icon.source}"/>`;
                break;
                case 'sprite':
                    template = `<svg><use xlink:href="${icon.source}"></use></svg>`;
                break;
                case 'xml':
                    template = icon.source;
                break;
            }
        }
        return template;
    }
    createSelection () {
        const iconSelectionArrow = this.createIcon(this.iconSelectionArrow);
        const getSelection = this.setSelection();
        return `
            ${!!getSelection.icon ? `<div class="wmselect__icon">${getSelection.icon}</div>` : ''}
            <div
                ${!!getSelection.title ? `title="${getSelection.title}"` : ''}
                class="wmselect__selected ${!!getSelection.icon ? 'wmselect__selected--calc-width' : ''}"
            >
                ${!this.selected.value.length ? `
                    <span class="wmselect__placeholder">
                        ${this.lang['placeholder']}
                    </span>
                ` : getSelection.selected}
            </div>
            <button type="button" class="wmselect__arrow">
                ${iconSelectionArrow.length ? iconSelectionArrow : Default.template.iconSelectionArrow}
            </button>
        `.trim();
    }
    createResult (options) {
        const tpl = [];
        const combineResult = (data) => {
            const setData = {};
            setData.text = data.text;
            if (!!data.img) setData.img = this.createIcon(data.img);
            let result = !!this.templateResult && this.templateResult(setData);
            if (!result) {
                result = typeof data.text == 'object' ? Object.values(data.text).join(' ') : data.text;
            }
            tpl.push(`<div data-value="${data.value}" class="wmselect__option">${result}</div>`);
        }
        if (!!options && !options.length) {
            tpl.push(`<span class="wmselect__result">${this.lang['lang-no-result']}</span>`);
        }
        else if (!!options && options.length) {
            options.map(item => {
                !!item.title && tpl.push(`<span class="wmselect__title">${item.title}</span>`);
                !!item.children && item.children.map(option => { combineResult(option); })
                if (!item.title && !item.children && !item.errors) { combineResult(item); }
                if (!!item.errors) {
                    tpl.push(`
                        <span class="wmselect__error">
                            ${!!item.message ? item.message : this.lang['lang-error-loading']}
                        </span>
                    `.trim());
                    for (let i = 0; i < item.errors.length; i++) {
                        console.error(`Error: ${item.errors[i].code}. Message: ${item.errors[i].message}`);
                    }
                }
            });
        }
        return tpl.join('');
    }
    createCustomSelect () {
        const templateOptions = this.createResult(this.getNativeOptions());
        const template = `
            <div class="wmselect ${this.selected.value.length ? 'change' : ''}" data-custom=${this.id}>
                <div class="wmselect__selection">${this.createSelection()}</div>
                <div class="wmselect__dropdown">
                    <div class="wmselect__inner">
                        ${this.config.search.turnOn ? `
                            <div class="wmselect__search">
                                <input
                                    type="text"
                                    class="wmselect__input"
                                    placeholder="${this.lang['search-placeholder']}"
                                >
                            </div>
                        ` : ''}
                        <div class="wmselect__options">${templateOptions}</div>
                    </div>
                </div>
            </div>
        `.trim();
        const createDom = new DOMParser().parseFromString(template, 'text/html').body.firstChild;
        this.native.after(createDom);
        return createDom;
    }
    listenerClick (e) {
        const isTargetSelection = e.target == this.dom.selection || this.dom.selection.contains(e.target);
        const isContainsActive = this.custom.classList.contains('active');
        if (isTargetSelection && !isContainsActive) this.wmopen();
        else if (isTargetSelection && isContainsActive) this.wmclose();

        const domParent = e.target.closest('.wmselect__option');
        const isInner = e.target == this.dom.inner || this.dom.inner.contains(e.target);
        const isOption = e.target.classList.contains('wmselect__option') || domParent;
        if (isInner && isOption) {
            this.wmchange((() => {
                if (domParent) return domParent.getAttribute('data-value');
                else return e.target.getAttribute('data-value');
            })());
            this.wmclose();
        }
    }
    listenerInput (e) {
        const value = e.target.value.trim();
        if ((value.length >= this.config.search.minLength) && !!this.ajax) {
            this.config.currentPage = 1;
            clearInterval(intervalInput);
            intervalInput = setInterval(() => {
                clearInterval(intervalInput);
                this.notice({ lang: 'searching', turnOn: true });
                this.asyncAjax({
                    value: value,
                    params: this.config.params,
                    apps: ({ data, html }) => {
                        this.options = data;
                        this.notice({ lang: 'searching', turnOn: false });
                        this.dom.options.scrollTop = 0;
                        this.dom.options.innerHTML = html;
                        this.dispatch('afterAjax');
                    }
                })
            }, this.config.intervalInput);
        }
    }
    listenerScroll (e) {
        const targetScrollTop = e.target.scrollTop;
        const pos = e.target.offsetHeight + targetScrollTop;
        const docLastOption = e.target.querySelector(`.wmselect__option:last-of-type`);

        const isPositionLastOption = !!docLastOption && docLastOption.offsetTop - e.target.offsetTop < pos;
        const isDirections = targetScrollTop > this.direction;
        const isPage = this.config.currentPage <= this.config.lastPage;

        if (isPositionLastOption && isDirections && isPage && this.stopInfinity && !!this.ajax) {
            this.stopInfinity = false;
            this.notice({ lang: 'loading-more', turnOn: true });
            this.asyncAjax({
                params: this.config.params,
                apps: ({ data, html }) => {
                    this.options.push(...data);
                    this.notice({ lang: 'loading-more', turnOn: false });
                    this.dom.options.insertAdjacentHTML('beforeEnd', html);
                    this.stopInfinity = true;
                    this.dispatch('afterAjax');
                    this.setActiveOption();
                }
            });
        }
        this.direction = targetScrollTop;
    }
    async asyncAjax ({ value, apps, params }) {
        const response = await new Promise((resolve, reject) => {
            this.ajax({
                native: this.native,
                currentPage: this.config.currentPage,
                value: value,
                resolve: resolve,
                parent: !!this.config.parent ? document.querySelector(this.config.parent) : undefined,
                params: params,
            });
        });
        const html = this.createResult(response.data);
        this.config.currentPage = response.currentPage;
        this.config.lastPage = response.lastPage;
        apps({ data: response.data, html });
        this.heightDropdown();
    }
    heightDropdown () {
        const showLine = !!this.config.showLine && this.config.showLine;
        if (showLine !== 'auto') {
            const defaultLine = showLine > 0 ? showLine : Default.showLine;
            let height = 0;
            for (let i = 0; i < defaultLine; i++) {
                const child = this.dom.options.childNodes[i];
                height += !!child ? child.offsetHeight : 0;
            }
            this.dom.options.style.maxHeight = `${height}px`;
        }
    }
    disabledSelect () {
        if (!!this.config.parent) {
            const dom = document.querySelector(this.config.parent);
            const index = dom.options.selectedIndex;
            const value = !!dom.options[index] && dom.options[index].value;
            if (value.length) {
                this.isDisabled = true;
                this.custom.classList.remove('disabled')
            }
            else {
                this.isDisabled = false;
                this.custom.classList.add('disabled');
            }
        }
    }
    notice ({ lang, turnOn }) {
        const regexp = /{@}/gi;
        const domNotice = this.custom.querySelector(`.wmselect__notice--${lang}`);
        const template = `<span class="wmselect__notice wmselect__notice--${lang}">{@}</span>`;
        switch(lang) {
            case 'searching':
                if (turnOn && !domNotice) {
                    this.dom.options.insertAdjacentHTML('beforebegin', template.replace(regexp, this.lang['lang-searching']));
                } else {
                    domNotice.remove();
                }
            break;
            case 'loading-more':
                if (turnOn && !domNotice) {
                    this.dom.options.insertAdjacentHTML('beforeEnd', template.replace(regexp, this.lang['lang-loading-more']));
                } else {
                    domNotice.remove();
                }
            break;
        }
    }
    dispatch (name) {
        switch (name) {
            case 'change': this.native.dispatchEvent(new CustomEvent("wmselect:change")); break;
            case 'open': this.native.dispatchEvent(new CustomEvent("wmselect:open")); break;
            case 'close': this.native.dispatchEvent(new CustomEvent("wmselect:close")); break;
            case 'afterAjax': this.native.dispatchEvent(new CustomEvent("wmselect:afterAjax")); break;
        }
    }
    wmreset (params = {}) {
        const iconSelection = this.createIcon(this.iconSelection);
        const activeOption = this.dom.options.querySelector('.wmselect__option.active');
        this.native.value = '';
        this.selected.value = '';
        this.selected.text = '';
        this.selected.img.type = '';
        this.selected.img.source = '';
        this.dom.selected.innerHTML = `<span class="wmselect__placeholder">${this.lang['placeholder']}</span>`;
        this.dom.selected.removeAttribute('title');
        this.custom.classList.remove('change');
        !!activeOption && activeOption.classList.remove('active');
        if (iconSelection.length && !!this.dom.icon) this.dom.icon.innerHTML = iconSelection;
        if (!!params.options && params.options === 'reset') {
            this.options.length = 0;
            this.native.innerHTML = '<option></option>';
            Array.from(this.custom.querySelectorAll('.wmselect__option, .wmselect__title'), children => {
                children.remove();
            })
        }
        params.disabled && this.custom.classList.add('disabled');
    }
    wmchange (value) {
        const resetActiveClass = () => {
            Array.from(this.custom.querySelectorAll('.wmselect__option'), option => {
                option.classList.remove('active');
            });
        }
        if (!!value) {
            let change = {};
            for (const key in this.options) {
                const children = this.options[key].children;
                if (!!children) {
                    for (const key2 in children) {
                        if (children[key2].value === value) {
                            change = children[key2];
                            break;
                        }
                    }
                }
                if (this.options[key].value === value) {
                    change = this.options[key];
                    break;
                }
            }
            if (!change.value) {
                console.error(Default.errors.change);
                return;
            }
            this.selected.img.type = !!change.img && !!change.img.type ? change.img.type : '';
            this.selected.img.source = !!change.img && !!change.img.source ? change.img.source : '';
            this.selected.value = change.value;
            this.selected.text = change.text;
            const getSelection = this.setSelection();
            this.dom.selected.innerHTML = getSelection.selected;
            this.dom.selected.setAttribute('title', getSelection.title);
            if (!!getSelection.icon && !!this.dom.icon) this.dom.icon.innerHTML = getSelection.icon;
            const option = this.native.querySelector(`option[value="${this.selected.value}"]`);
            if (!!option) { option.selected = true; }
            else {
                this.native.innerHTML = `
                    <option value="${this.selected.value}">${
                        typeof this.selected.text == 'object' ? Object.values(this.selected.text).join(' ') : this.selected.text
                    }</option>
                `.trim();
            }
            resetActiveClass();
            this.custom.classList.add('change');
            this.custom.querySelector(`.wmselect__option[data-value="${change.value}"]`).classList.add('active');
            this.dispatch('change');
            return;
        }
        if (value === undefined) {
            this.dom.selected.innerHTML = `
                <span class="wmselect__placeholder">
                    ${this.lang['placeholder']}
                </span>
            `.trim();
            resetActiveClass();
            this.dom.selected.removeAttribute('title');
            this.native.value = '';
            this.selected.value = '';
            this.selected.text = '';
            return;
        }
        console.error(Default.errors.changeEmpty);
    }
    wmclose () {
        if (this.config.search.turnOn) this.dom.input.value = '';
        this.custom.classList.remove('active');
        this.dom.dropdown.classList.add('animate');
        this.dispatch('close');
        this.dom.dropdown.addEventListener('transitionend', e => {
            e.target.classList.remove('animate');
        })
    }
    wmopen (params) {
        Array.from(document.querySelectorAll('.wmselect'), select => {
            select.classList.remove('active');
        })
        this.custom.classList.add('active');
        this.disabledSelect();
        this.setActiveOption();
        this.dispatch('open');
        if (!!params) {
            this.config.params = params;
        }
        if ((!this.options.length || this.options.length || !!this.config.params) && !!this.ajax) {
            this.config.currentPage = 1;
            this.dom.options.innerHTML = '';
            this.notice({ lang: 'loading-more', turnOn: true });
            this.heightDropdown();
            this.asyncAjax({
                params: this.config.params,
                apps: ({ data, html }) => {
                    this.options = data;
                    this.notice({ lang: 'loading-more', turnOn: false });
                    this.dom.options.innerHTML = html;
                    this.setActiveOption();
                    this.dispatch('afterAjax');
                }
            });
            return;
        }
        this.heightDropdown();
    }
}