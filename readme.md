# WMSelect
Бібліотека від компанії [`WebMaestro`](https://webmaestro.com.ua/).
#### Встановлення
Встановити пакет можна за допомогою команди:
```
npm i --save wmselect
```
#### Підключення

#####  Стилі:
```html
<link rel="stylesheet" href="dist/wmselect.bundle.css">
```
або
```css
@import 'wmselect/dist/wmselect.bundle.css';
```
Детальніше про стилізацію WMSelect в розділі **Стилізація**.
#####  Скрипти:
```html
<script src="dist/wmselect.bundle.js"></script>
```
або
```javascript
import WMSelect from 'wmselect';
```

#### Ініціалізація
**1 param** - елемент node.
**2 param** - об'єкт з вхідними даними.
```javascript
new WMSelect(document.getElementById('select'), {})
```
#### Вхідні дані
**showLine** - ***Number***, за замовчуванням *5*.
**Опис.** Відповідає за висоту випадаючого блоку, з урахуванням заголовків, і опцій.

**search** - приймає дві властивості ***{ turnOn: Boolean, minLength: Number }***.
***- turnOn*** - за замовчуванням ***false***.
***- minLength*** - за замовчуванням ***0***.
**Опис.** Відповідає за підключення пошуку. Властивість ***minLength*** - мінімальна кількість символів для здійснення пошуку. Не рекомендується, так як пошук не буде здійснюватись якщо користувач видалить пошуковий запит з форми.

**intervalInput** - ***Number***, вимірюється в ***ms***, за замовчуванням ***800***.
**Опис.** Зупинка запитів на сервер в момент введення користувачем у форму даних.

**parentDisabled** - ***String***, немає значеннь за замовчуванням.
**Опис.** Селектор батьківсього select, для взаємозв'язку. Даний параметр слідкує за батьківським select, і відповідно його стану блокує наявний select, або розблоковує.

**reverseIconSelection** - ***Boolean***, за замовчуванням ***false***.
**Опис.** Відповідає за зміну зображення в selection при виборі опцій з зображенням.
***Зауваження:***
Зображення не буде змінним, якщо не вказати початковий стан властовисоті ***iconSelection***.

**langSelector** - ***String***, немає значеннь за замовчуванням.
**Опис.** Властивість приймає selector, у якому описана локалізація data атрибутів. Потрібно зауважити, data атрибути описані в select мають більший пріорітет.

**iconSelection** - приймає дві властивості ***{ type: String, source: String }***.
***- type*** - наявні три формата: ***img***, ***sprite***, ***xml***.
***- source*** - приймає значення в залежності від формата: ***img*** - приймає url; ***sprite*** - приймає url; ***xml*** - приймає сам код xml.
**Опис.** Відповідає за зображення в selection.

**iconSelectionArrow** - приймає дві властивості ***{ type: String, source: String }***, за замовчуванням ***xml*** іконка.
***- type*** - наявні три формата: ***img***, ***sprite***, ***xml***.
***- source*** - приймає значення в залежності від формата: ***img*** - приймає url; ***sprite*** - приймає url; ***xml*** - приймає сам код xml.
**Опис.** Відповідає за зміну іконки в кнопці.

**templateResult** - ***callback***, який приймає ***Object***, і повинен повернути ***html*** шаблон тип якого ***String***.
Приймає ***text / img***, в залежності від даних.
**Опис.** Властивість призначена для створення свого шаблону результатів, які будуть у випадаючому списку.
**Приклад:**
```javascript
templateResult (state) {
    return `
        ${!!state.img ? state.img : ''}
        <span class="static-text">${state.text}</span>
    `.trim();
}
```

**templateSelection** - ***callback***, який приймає ***Object***, і повинен повернути ***html*** шаблон тип якого ***String***.
**Опис.** Подібний до властивості ***templateResult***. В залежності даних, потрібно формувати свій шаблон. Не приймає ***img***, він формується динамічно якщо властивість ***reverseIconSelection*** має значення ***true***.
Властивість призначена для створення свого шаблону в selection.

**ajax** - ***callback***, який приймає ***Object*** з властивостями:
***- native*** - нативний select над яким здійснюється запит.
***- currentPage*** - номер наявної сторінки.
***- value*** - введене значення з форми користувачем.
***- params*** - додаткові параметри, які передаються за домогою метода ***open*** детальніше в розділі ***методи***.
***- resolve*** - функція, яка приймає результат ajax запиту.
**Опис.** Властивість описує всі наявні запити для ***прокрутки***,  ***пошуку***, ***початкової загрузки даних***. Працює в зв'язці з бібліотекою axios.
**Важливо!**
Якщо ви описуєте опції в html, і є в наявності функціонал "прокрутки", тоді потрібно вказати атрибут [data-default-lastpage] зі значенням кількості сторінок.
```javascript
ajax ({ native, currentPage, value, params, resolve }) {
    axios.get('http://json').then(response => {
        resolve(response.data);
    })
}
```
#### Методи
Варіанти роботи над методами:
```javascript
document.getElementById('select').wmopen(); // з нативного елемента
// або
init.wmopen(); // з екземпляра
```

**wmopen({ dataAjax: [Object, String, Number ...] })** - метод приймає ***Object***.
***- dataAjax*** - властивість, яка призначена для передачі даних у параметр ***params*** функції ***ajax*** та приймає будь яке значення будь якого типу.
**Опис.** Сам метод відкриває випадаючий список.
```javascript
document.getElementById('models').wmopen({
    dataAjax: e.target.value,
});
```

**wmclose()** - метод закриває випадаючий список.

**wmchange(String)** - приймає значення ***value*** типом ***String***.
**Опис.** Спрацьовує, якщо дані уже підвантажились, в противному випадку буде попередження в консолі. Якщо нічого не передати, в результаті скинемо вибраний елемент.

**wmreset({ disabled: true, options: 'reset' })** - приймає властивість ***disabled*** зі значенням ***true***.
***- disabled*** - дана властивість робить не активним select. Немає потреби ставити значення false, просто не передаємо у метод нічого.
Виконує такі дії:
***1.*** вертає початковий стан зображення selection, якщо він був налаштований.
***2.*** скидує вибраний елемент в кастомного і нативного select, і повертає placeholder.
***- options*** - потрібно передати значення reset тип якого String:
Виконує такі дії:
***1.*** видаляє всі опції.
```javascript
document.getElementById('models').wmreset();
or
document.getElementById('models').wmreset({ disabled: true }); // блокує select
```
#### Події
Прив'язка подій здійснюється на нативний елемент (не екземпляра).

**wmselect:open** - подія спрацьовує коли здійснюється відкриття випадаючого списку з результатами.
```javascript
document.getElementById('models').addEventListener('wmselect:open', (e) => { })
```
**wmselect:close** - подія спрацьовує коли здійснюється закриття випадаючого списку з результатами.
```javascript
document.getElementById('models').addEventListener('wmselect:close', (e) => { })
```
**wmselect:change** - подія спрацьовує коли вибране значення.
```javascript
document.getElementById('models').addEventListener('wmselect:change', (e) => { })
```
**wmselect:afterAjax** - подія спрацьовує коли ajax запити завершився, і сформувало dom з новими результатами.
```javascript
document.getElementById('models').addEventListener('wmselect:afterAjax', (e) => { })
```
#### data атрибути
***Локалізація:***

**[data-placeholder]** - placeholder самого selection.
**[data-search-placeholder]** - placeholder для форми пошуку.
**[data-lang-searching]** - процес пошуку, виводиться між початком пошукового запиту та до його завершення.
**[data-lang-loading-more]** - підвантаження, спрацьовує при лінивій загрузці (infinity). Додається в кінець всіх опцій на початку запиту та до його завершення.
**[data-lang-no-result]** - виводиться при відсутності результатів.
**[data-lang-error-loading]** - виводиться при наявності помилки, якщо в json прийшло повідомлення помилки, тоді даний атрибут ігнорується.

***Конфігураційні атрибути:***

**[data-default-lastpage]** - результатом даного атрибуту є кількість сторінок, яка потрібна для лінивої прокрутки, якщо здійснюється запит при відкритті, тоді цей атрибут не потрібен.

***Атрибути, які описують початковий selected:***

**[data-default-type]** - приймає тип зображення: ***img***, ***sprite***, ***xml***.
**[data-default-source]** - приймає url зображення або xml.
**[data-default-...]** - додаткові параметри, і є вхідними даними для "templateResult" і "templateSelection".

**Приклад:**
```html
<select
	name="example"
    id="example"
    data-placeholder="Виберіть значення"
    data-default-lastpage="5"
>
	<option
    	value="val-1"
        data-default-type="sprite"
        data-default-source="img/sprite.svg#example"
        data-default-years="2021"
   	>
    	Example value
    </option>
</select>
```
#### JSON
Простий шаблон:
```json
{
	"currentPage": "2",
    "lastPage": "4",
    "data": [
    	{
            "img": {
                "type": "img",
                "source": "/img/audi.png",
            },
            "value": "1",
            "text": "AUDI",
        },
        {
            "value": "2",
            "text": "BMW",
        },
        {
            "value": "3",
            "text": {
                "text": "CHEVROLET",
                "years": "2021",
            },
        },
    ]
}
```
Шаблон з заголовками:
```json
{
	"currentPage": "2",
    "lastPage": "4",
    "data": [
    	{
            "title": "Група авто на букву А",
            "children": [
                {
                    "img": {
                        "type": "sprite",
                        "source": "/img/sprite.svg#arrow",
                    },
                    "value": "a1",
                    "text": "AUDI",
                },
                {
                    "value": "a2",
                    "text": "ALFA ROMEO",
                },
            ]
        },
    ]
}
```
Комбінування шаблонів:
```json
{
	"currentPage": "2",
    "lastPage": "4",
    "data": [
    	{
            "title": "Комбінуємо",
            "children": [
                {
                    "value": "mdx",
                    "text": "MDX"
                },
                {
                    "value": "rl",
                    "text": "RL"
                },
            ]
        },
        {
            "value": "static",
            "text": {
                "text": "Статичний текст",
                "pages": "2",
            }
        }
    ]
}
```
Шаблон без результатів:
```json
{
	"currentPage": "0",
    "lastPage": "0",
    "data": []
}
```
Шаблон з помилками:
```json
{
	"currentPage": "0",
    "lastPage": "0",
    "data": [
    	{
        	"message": "Errors 500 and 404",
            "errors": [
            	{
                	"code": "500",
                    "message": "Error 500"
                },
                {
                	"code": "404",
                    "message": "Error 404"
                }
            ]
        }
    ]
}
```
#### Стилізація
Потрібно добавити до нативного select клас **wmselect-custom**, як це зроблено в прикладі html розділу **Приклади**. У ньому є ряд властивостей:
```css
.wmselect-custom {
    width: 100%;
    height: var(--height-select);
    opacity: 0;
    pointer-events: none;
}
```
Детальніше про змінні в css:
```css
:root {
    --color-border-search: #d1d1d1; /* колір рамки форми пошуку */
    --color-scrollbar: #d1d1d1; /* колір скроллбара */
    --color-svg: #555; /* колір стрілки svg, по default це xml, тому дана властивість буде працювати */
    --color-placeholder: #bababa; /* колір placeholder у формі пошуку */
    --color-option-selected: green; /* колір вибраного значення у випадаючому вікні */
    --color-notice: #8e8e8e; /* колір сповіщень, такий як [data-lang-loading-more], [data-lang-searching] */
    --color-border-error: #f71515; /* колір рамки при помилці валідації */
    --color-text-error: #f71515; /* колір тексту при помилці валідації  */
    --bg-disabled: #ececec; /* фон не активного селекта (disabled) */
    --bg-arrow: #eee; /* фон кнопки зі стрілкою */
    --height-select: 52px; /* висота, ця властивість потрібна для нативного і кастомного селекта */
    --box-shadow: 0px 1px 3px #777777; /* тінь для кастомного селекта */
    --border-option: 1px solid #eee; /* рамка розділювач між опціями */
    --width-scrollbar: 4px; /* ширина скролбара */
    --width-icon: 40px; /* ширина іконки, по даній властивості обраховується ширина самого контенту з текстом */
    --font-size: 16px; /* розмір шрифта */
    --font-size-search: 14px; /* розмір шрифта пошуку і placeholder */
}
```
Всі властивості можна перевизначати у себе в коді. Для прикладу потрібно змінити колір рамки пошуку, тому достатньо у себе описати псевдосклас :root з властивістю --color-border-search: black.
```css
@import 'wmselect/dist/wmselect.bundle.css';
:root {
    --color-border-search: black;
}
```
#### Приклади
##### HTML
```html
<select
	name="example"
    id="example"
    class="wmselect-custom"
    data-placeholder="Change"
    data-search-placeholder="Search"
    data-lang-error-loading="Error"
    data-lang-loading-more="Loader"
    data-lang-no-result="Not result"
    data-lang-searching="Searching..."
    data-default-lastpage="5"
>
	<option
    	value="val-1"
        data-default-type="sprite"
        data-default-source="img/sprite.svg#example"
        data-default-years="2021"
   	>
    	Example value
    </option>
</select>
```
##### JAVASCRIPT
```javascript
import WMSelect from 'wmselect';
import axios from 'axios';

const selector = document.getElementById('#example');

const initExample = new WMSelect(selector, {
    showLine: 5,
    search: {
        turnOn: false,
        minLength: 0,
    },
    intervalInput: 800,
    parentDisabled: '', // слухає інший селектор, прив'язує його
    reverseIconSelection: false,
    langSelector: '', // передаємо селектор, з якого буде братись локалізація
    iconSelection: {
        type: 'img',
        source: 'img/tick.png',
    },
    iconSelectionArrow: {
        type: 'sprite',
        source: 'img/sprite.svg#arrow',
    },
    templateSelection (state) {
        console.log(state);
        return '<span>Template selection</span>';
    },
    templateResult (state) {
        console.log(state);
        return '<span>Template result</span>';
    },
    ajax (params) {
        console.log(params.native); // нативний елемент
        console.log(params.currentPage); // активна сторінка
        console.log(params.value); // пошуковий запит
        console.log(params.params); // свої параметри
        axios.get('https://url').then(response => {
            params.resolve(response.data); // функція, в яку треба передати результат запиту
        })
    }
});

selector.addEventListener('wmselect:change', (e) => {
    console.log(e.target);
    // спрацьовує кожен раз коли щось обрали
})

selector.addEventListener('wmselect:open', (e) => {
    console.log(e.target);
    // спрацьовує кожен раз коли відкрили кастомний селект
})

selector.addEventListener('wmselect:close', (e) => {
    console.log(e.target);
    // спрацьовує кожен раз коли закрили кастомний селект
})

selector.addEventListener('wmselect:afterAjax', (e) => {
    console.log(e.target);
    // спрацьовує після завершення запиту на сервер і формування результатів
})

// "Методи

selector.wmopen(); // відкриваємо селект
selector.wmopen({ // відкриваємо селект та робим запит на сервер з даними які передали в dataAjax
    dataAjax: e.target.value,
});

selector.wmclose(); // закриває селект

selector.wmchange('value') // отримує value опції, яку треба обрати

selector.wmreset() // скидує нативний і кастомний селект
selector.wmreset({ disabled: true }) // скидує нативний і кастомний селект і додатково його блокує, робить сірим
```

